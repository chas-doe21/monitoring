# Why

The only thing worse than having a system go down, is having a system go down *and not knowing it*.

Knowing a system is down is the first requisite for bringing it up again. Nobody will fix it if nobody knows it's broken.

A good monitoring system **and** a good process around incident handling makes often the difference between a breached and unbreached SLA.


## Monitoring systems

A good monitoring system will allow us to promptly know when something's not right.

There are a plethora of systems around, both FOSS and proprietary.


## Alerts

Some systems are so business-critical, and have such strict SLAs, that they have a team actively checking and reacting to alarms from the monitoring system around-the-clock, all days of the year.

Other systems may be less strict, and less critical. They are often still monitored at all times, but the alarms from the monitoring system are only actively checked during office hours.


## Reacting to alerts

A monitoring system by itself isn't enough: unless someone is checking the alarm page at all times, automatic alarms needs to be dispatched to allow us to have observability.

Such alarms may be delivered by mail, sms, mobile notification, etc.

It's good to follow the goldilocks rule on which alarms to dispatch: it's a balance between observability (which problems can we safely ignore until the next office day?) and keeping it relevant (if I get 50 emails with alarms every day, I get none since I'll ignore them after the first day).

Usually some form of **severity** scale is used to automatically dispatch alarms based on it.


# Where

Your monitoring system shouldn't run on the same server(s) you're monitoring: if the server(s) go down, so does your monitoring system (remember scenario 1 from the HA-lesson?).

Ideally (if the scale of operations make sense, and your tool supports it) you should cluster your monitoring system to achieve some redundancy.


## Reachability

Keep in mind that in order to monitor your servers, the monitoring system need to be able to reach them.

If you for example deploy everything locally and your monitoring on the cloud, some form of VPN should be in place to allow the monitor to reach the internal services.


## Outsourcing monitoring

Some companies buy monitoring as a service. This means their servers and services are monitored by a third-party supplier.

It's good to know who takes care of monitoring even when not working directly with it: as an ops person, you need to know who you need to contact in case you need to take temporarily take down a service, or move it.

It's good practice to let the monitoring people know of upcoming operations that will trigger alarms, so to spare them the handling of a needless incident.

This may be a matter of courtesy when working with internal monitoring; but a financial matter when working with an external monitoring solution (usually monitoring-as-a-service charges extra for each handled alarm).


# How

But how does monitoring *actually* work?

There are, roughly speaking, two kinds of monitoring: active and passive.


## Passive monitoring

Passive monitoring is, as the name implies, based on observations that don't need the monitored server to actively do anything outside its normal operations.

Example of passive checks:

-   can I ping the host?
-   is the https port reachable and answering?
-   &#x2026;


## Active monitoring

Usually active monitoring requires the installation of an `agent` in the target server to be monitored. This agent talks back to the monitoring server, giving information about different parameters (usually configurable).

Examples of data actively collected via an agent:

-   How much memory is the system using?
-   What's the CPU load?
-   How much disk space is there available?
-   When was `/etc/passwd` last modified?
-   How many docker containers are running in the machine
-   &#x2026;


## Let's complicate things a bit

To make matter less clear, different systems use the terms "active monitoring" and "passive monitoring" in other ways.

For example, Zabbix defines active and passive checks as:

In a passive check the agent responds to a data request. Zabbix server (or proxy) asks for data, for example, CPU load, and Zabbix agent sends back the result.

Active checks require more complex processing. The agent must first retrieve a list of items from Zabbix server for independent processing. Then it will periodically send new values to the server.


## Let's complicate things a bit more

Lately Prometheus has been gaining popularity in the monitoring game.

It's not based on a server-agent paradigm, but uses instead a time-series database to collect stats at regular intervals from an HTTP endpoint.


# Alarms

Let's dive deeper in the world of monitoring alarms.

![img](./noc.jpg)


## Generic checks

Most monitoring solutions come pre-loaded with default checks they perform, to name a few (from Zabbix):

-   hardware: CPU/MEM/disk IO
-   network: throughput, traffic
-   system: uptime, users

Those are usually a good start, and cover a lot of scenarios (load too high, memory too low, disk too low, network bottleneck, system compromise, &#x2026;).


## Specific checks

On top of generic checks, we can define custom checks, both by loading certain templates (i.e. Docker template to keep track of containers) and by defining application checks (i.e. check a certain service on a certain port for availability, status code, presence of a string or header in the response, &#x2026;)

Together with generic checks, specific checks allow us to cover even more scenarios.


# Tooling

The rest of this lesson will be 2 practical labs we'll use to set up monitoring for some custom service.


# Labb 1: Zabbix

Let's install [Zabbix](https://www.zabbix.com) on a VM.


## Zabbix Overview

[See here](https://www.zabbix.com/documentation/current/en/manual/introduction/overview) for the official documentation.


## Install Zabbix

In [this Vagrantfile](./zabbix/Vagrantfile) we can find a 2-VM setup we'll use to install Zabbix.

We'll start with a `vagrant up`, and later [check the docs for installation instructions](https://www.zabbix.com/documentation/current/en/manual/installation/install_from_packages#From-distribution-packages).


## Add a new host

We'll add a new host by creating a host in the server, and installing and configuring the agent on the target host.


## Trigger a problem

Next, we'll trigger a problem manually.


## Create a web scenario

We'll create a check for an http service on our target host.

Then we'll start the http service, and check that the web scenario works.

We'll create a trigger for the web scenario, to trigger a problem when the scenario fails, and try to stop the http service to trigger a problem.


## Set up alarms via mail

We'll set up Zabbix to deliver alarms via email.

We'll need to:

-   setup a new media
-   configure the media for our user
-   activate the trigger action under Configuration -> Actions
-   trigger a problem


# Labb 2: Prometheus & Grafana

Let's install [Prometheus](https://prometheus.io) on a VM.


## Prometheus Overview

[See here](https://prometheus.io/docs/introduction/overview/) for the official documentation.


## Install Prometheus

See [the docs](https://prometheus.io/docs/introduction/first_steps/) for installation instructions.

We'll install Prometheus and Grafana.


## Add Zabbix as a source to Grafana

Grafana is a very flexible visualization framework.

We'll install a plugin to allow us to view data from Zabbix in Grafana.


# To be continued

We'll work more with monitoring in the coming lessons.