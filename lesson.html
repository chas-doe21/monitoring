<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Monitoring</title>
<meta name="author" content=""/>
<meta name="description" content=""/>
<meta name="keywords" content=""/>
<style type="text/css">
.underline { text-decoration: underline; }
</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/reveal.js/dist/reveal.css"/>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/reveal.js/dist/theme/moon.css" id="theme"/>


<!-- If the query includes 'print-pdf', include the PDF print sheet -->
<script>
    if( window.location.search.match( /print-pdf/gi ) ) {
        var link = document.createElement( 'link' );
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = 'https://cdn.jsdelivr.net/npm/reveal.js/css/print/pdf.css';
        document.getElementsByTagName( 'head' )[0].appendChild( link );
    }
</script>
</head>
<body>
<div class="reveal">
<div class="slides">
<section id="sec-title-slide"><h1 class="title">Monitoring</h1><p class="subtitle"></p>
<p class="date">Created: 2022-04-22 Fri 23:50</p>
</section>
<section>
<section id="slide-orgff7136b">
<h2 id="orgff7136b">Why</h2>
<p>
The only thing worse than having a system go down, is having a system go down
<i>and not knowing it</i>.
</p>

<p>
Knowing a system is down is the first requisite for bringing it up again. Nobody
will fix it if nobody knows it's broken.
</p>

<p>
A good monitoring system <b>and</b> a good process around incident handling makes
often the difference between a breached and unbreached SLA.
</p>
</section>
<section id="slide-org0c0485d">
<h3 id="org0c0485d">Monitoring systems</h3>
<p>
A good monitoring system will allow us to promptly know when something's not
right.
</p>

<p>
There are a plethora of systems around, both FOSS and proprietary.
</p>
</section>
<section id="slide-org743bd39">
<h3 id="org743bd39">Alerts</h3>
<p>
Some systems are so business-critical, and have such strict SLAs, that they have
a team actively checking and reacting to alarms from the monitoring system
around-the-clock, all days of the year.
</p>

<p>
Other systems may be less strict, and less critical. They are often still
monitored at all times, but the alarms from the monitoring system are only
actively checked during office hours.
</p>
</section>
<section id="slide-org91a92c1">
<h3 id="org91a92c1">Reacting to alerts</h3>
<p>
A monitoring system by itself isn't enough: unless someone is checking the alarm
page at all times, automatic alarms needs to be dispatched to allow us to have
observability.
</p>

<p>
Such alarms may be delivered by mail, sms, mobile notification, etc.
</p>

<p>
It's good to follow the goldilocks rule on which alarms to dispatch: it's a
balance between observability (which problems can we safely ignore until the
next office day?) and keeping it relevant (if I get 50 emails with alarms every
day, I get none since I'll ignore them after the first day).
</p>

<p>
Usually some form of <b>severity</b> scale is used to automatically dispatch alarms
based on it.
</p>
</section>
</section>
<section>
<section id="slide-orgc364b0e">
<h2 id="orgc364b0e">Where</h2>
<p>
Your monitoring system shouldn't run on the same server(s) you're monitoring:
if the server(s) go down, so does your monitoring system (remember scenario 1
from the HA-lesson?).
</p>

<p>
Ideally (if the scale of operations make sense, and your tool supports it) you
should cluster your monitoring system to achieve some redundancy.
</p>
</section>
<section id="slide-org9e0bcea">
<h3 id="org9e0bcea">Reachability</h3>
<p>
Keep in mind that in order to monitor your servers, the monitoring system need
to be able to reach them.
</p>

<p>
If you for example deploy everything locally and your monitoring on the cloud,
some form of VPN should be in place to allow the monitor to reach the internal
services.
</p>
</section>
<section id="slide-orgf4264f0">
<h3 id="orgf4264f0">Outsourcing monitoring</h3>
<p>
Some companies buy monitoring as a service. This means their servers and
services are monitored by a third-party supplier.
</p>

<p>
It's good to know who takes care of monitoring even when not working directly
with it: as an ops person, you need to know who you need to contact in case you
need to take temporarily take down a service, or move it.
</p>

<p>
It's good practice to let the monitoring people know of upcoming operations that
will trigger alarms, so to spare them the handling of a needless incident.
</p>

<p>
This may be a matter of courtesy when working with internal monitoring; but a
financial matter when working with an external monitoring solution (usually
monitoring-as-a-service charges extra for each handled alarm).
</p>
</section>
</section>
<section>
<section id="slide-org11280f9">
<h2 id="org11280f9">How</h2>
<p>
But how does monitoring <i>actually</i> work?
</p>

<p>
There are, roughly speaking, two kinds of monitoring: active and passive.
</p>
</section>
<section id="slide-orgc5be37e">
<h3 id="orgc5be37e">Passive monitoring</h3>
<p>
Passive monitoring is, as the name implies, based on observations that don't
need the monitored server to actively do anything outside its normal operations.
</p>

<p>
Example of passive checks:
</p>

<ul>
<li>can I ping the host?</li>
<li>is the https port reachable and answering?</li>
<li>&#x2026;</li>

</ul>
</section>
<section id="slide-org94eeaa6">
<h3 id="org94eeaa6">Active monitoring</h3>
<p>
Usually active monitoring requires the installation of an <code>agent</code> in the target
server to be monitored. This agent talks back to the monitoring server, giving
information about different parameters (usually configurable).
</p>

<p>
Examples of data actively collected via an agent:
</p>

<ul>
<li>How much memory is the system using?</li>
<li>What's the CPU load?</li>
<li>How much disk space is there available?</li>
<li>When was <code>/etc/passwd</code> last modified?</li>
<li>How many docker containers are running in the machine</li>
<li>&#x2026;</li>

</ul>
</section>
<section id="slide-orgb48e433">
<h3 id="orgb48e433">Let's complicate things a bit</h3>
<p>
To make matter less clear, different systems use the terms "active monitoring"
and "passive monitoring" in other ways.
</p>

<p>
For example, Zabbix defines active and passive checks as:
</p>

<p>
In a passive check the agent responds to a data request. Zabbix server (or
proxy) asks for data, for example, CPU load, and Zabbix agent sends back the
result.
</p>

<p>
Active checks require more complex processing. The agent must first retrieve a
list of items from Zabbix server for independent processing. Then it will
periodically send new values to the server.
</p>
</section>
<section id="slide-orgf17f257">
<h3 id="orgf17f257">Let's complicate things a bit more</h3>
<p>
Lately Prometheus has been gaining popularity in the monitoring game.
</p>

<p>
It's not based on a server-agent paradigm, but uses instead a time-series
database to collect stats at regular intervals from an HTTP endpoint.
</p>
</section>
</section>
<section>
<section id="slide-org44eb2e2">
<h2 id="org44eb2e2">Alarms</h2>
<p>
Let's dive deeper in the world of monitoring alarms.
</p>


<div id="orgbb85f29" class="figure">
<p><img src="./noc.jpg" alt="noc.jpg" />
</p>
</div>
</section>
<section id="slide-org856c77f">
<h3 id="org856c77f">Generic checks</h3>
<p>
Most monitoring solutions come pre-loaded with default checks they perform, to
name a few (from Zabbix):
</p>

<ul>
<li>hardware: CPU/MEM/disk IO</li>
<li>network: throughput, traffic</li>
<li>system: uptime, users</li>

</ul>

<p>
Those are usually a good start, and cover a lot of scenarios (load too high,
memory too low, disk too low, network bottleneck, system compromise, &#x2026;).
</p>
</section>
<section id="slide-orgf3d6450">
<h3 id="orgf3d6450">Specific checks</h3>
<p>
On top of generic checks, we can define custom checks, both by loading certain
templates (i.e. Docker template to keep track of containers) and by defining
application checks (i.e. check a certain service on a certain port for
availability, status code, presence of a string or header in the response, &#x2026;)
</p>

<p>
Together with generic checks, specific checks allow us to cover even more
scenarios.
</p>
</section>
</section>
<section>
<section id="slide-org1641a97">
<h2 id="org1641a97">Tooling</h2>
<p>
The rest of this lesson will be 2 practical labs we'll use to set up monitoring
for some custom service.
</p>
</section>
</section>
<section>
<section id="slide-orgc26c7da">
<h2 id="orgc26c7da">Labb 1: Zabbix</h2>
<p>
Let's install <a href="https://www.zabbix.com">Zabbix</a> on a VM.
</p>
</section>
<section id="slide-org8bb113b">
<h3 id="org8bb113b">Zabbix Overview</h3>
<p>
<a href="https://www.zabbix.com/documentation/current/en/manual/introduction/overview">See here</a> for the official documentation.
</p>
</section>
<section id="slide-org5b1daa4">
<h3 id="org5b1daa4">Install Zabbix</h3>
<p>
In <a href="./zabbix/Vagrantfile">this Vagrantfile</a> we can find a 2-VM setup we'll use to install Zabbix.
</p>

<p>
We'll start with a <code>vagrant up</code>, and later
<a href="https://www.zabbix.com/documentation/current/en/manual/installation/install_from_packages#From-distribution-packages">check
the docs for installation instructions</a>.
</p>
</section>
<section id="slide-orgd1b6d37">
<h3 id="orgd1b6d37">Add a new host</h3>
<p>
We'll add a new host by creating a host in the server, and installing and
configuring the agent on the target host.
</p>
</section>
<section id="slide-orgd65bb4b">
<h3 id="orgd65bb4b">Trigger a problem</h3>
<p>
Next, we'll trigger a problem manually.
</p>
</section>
<section id="slide-org417d78c">
<h3 id="org417d78c">Create a web scenario</h3>
<p>
We'll create a check for an http service on our target host.
</p>

<p>
Then we'll start the http service, and check that the web scenario works.
</p>

<p>
We'll create a trigger for the web scenario, to trigger a problem when the
scenario fails, and try to stop the http service to trigger a problem.
</p>
</section>
<section id="slide-orgd928dc7">
<h3 id="orgd928dc7">Set up alarms via mail</h3>
<p>
We'll set up Zabbix to deliver alarms via email.
</p>

<p>
We'll need to:
</p>

<ul>
<li>setup a new media</li>
<li>configure the media for our user</li>
<li>activate the trigger action under Configuration -&gt; Actions</li>
<li>trigger a problem</li>

</ul>
</section>
</section>
<section>
<section id="slide-org5ad6c28">
<h2 id="org5ad6c28">Labb 2: Prometheus &amp; Grafana</h2>
<p>
Let's install <a href="https://prometheus.io">Prometheus</a> on a VM.
</p>
</section>
<section id="slide-orgfe7d3fb">
<h3 id="orgfe7d3fb">Prometheus Overview</h3>
<p>
<a href="https://prometheus.io/docs/introduction/overview/">See here</a> for the official documentation.
</p>
</section>
<section id="slide-org2583a62">
<h3 id="org2583a62">Install Prometheus</h3>
<p>
See <a href="https://prometheus.io/docs/introduction/first_steps/">the docs</a> for installation instructions.
</p>

<p>
We'll install Prometheus and Grafana.
</p>
</section>
<section id="slide-org56e8c54">
<h3 id="org56e8c54">Add Zabbix as a source to Grafana</h3>
<p>
Grafana is a very flexible visualization framework.
</p>

<p>
We'll install a plugin to allow us to view data from Zabbix in Grafana.
</p>
</section>
</section>
<section>
<section id="slide-orgd4cf883">
<h2 id="orgd4cf883">To be continued</h2>
<p>
We'll work more with monitoring in the coming lessons.
</p>
</section>
</section>
</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/reveal.js/dist/reveal.js"></script>

<script>
// Full list of configuration options available here:
// https://github.com/hakimel/reveal.js#configuration
Reveal.initialize({

// Optional libraries used to extend on reveal.js
dependencies: [
 { src: 'https://cdn.jsdelivr.net/npm/reveal.js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
 { src: 'https://cdn.jsdelivr.net/npm/reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
 { src: 'https://cdn.jsdelivr.net/npm/reveal.js/plugin/zoom-js/zoom.js', async: true, condition: function() { return !!document.body.classList; } },
 { src: 'https://cdn.jsdelivr.net/npm/reveal.js/plugin/notes/notes.js', async: true, condition: function() { return !!document.body.classList; } }]

});

</script>
</body>
</html>
