% Created 2022-04-22 Fri 23:52
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\date{}
\title{Monitoring}
\hypersetup{
 pdfauthor={},
 pdftitle={Monitoring},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.5.2)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Why}
\label{sec:org8f8c8d8}
The only thing worse than having a system go down, is having a system go down
\emph{and not knowing it}.

Knowing a system is down is the first requisite for bringing it up again. Nobody
will fix it if nobody knows it's broken.

A good monitoring system \textbf{and} a good process around incident handling makes
often the difference between a breached and unbreached SLA.
\subsection*{Monitoring systems}
\label{sec:org6b0cf35}
A good monitoring system will allow us to promptly know when something's not
right.

There are a plethora of systems around, both FOSS and proprietary.
\subsection*{Alerts}
\label{sec:org72a330e}
Some systems are so business-critical, and have such strict SLAs, that they have
a team actively checking and reacting to alarms from the monitoring system
around-the-clock, all days of the year.

Other systems may be less strict, and less critical. They are often still
monitored at all times, but the alarms from the monitoring system are only
actively checked during office hours.
\subsection*{Reacting to alerts}
\label{sec:orgaf17cf9}
A monitoring system by itself isn't enough: unless someone is checking the alarm
page at all times, automatic alarms needs to be dispatched to allow us to have
observability.

Such alarms may be delivered by mail, sms, mobile notification, etc.

It's good to follow the goldilocks rule on which alarms to dispatch: it's a
balance between observability (which problems can we safely ignore until the
next office day?) and keeping it relevant (if I get 50 emails with alarms every
day, I get none since I'll ignore them after the first day).

Usually some form of \textbf{severity} scale is used to automatically dispatch alarms
based on it.
\section*{Where}
\label{sec:org2dc8446}
Your monitoring system shouldn't run on the same server(s) you're monitoring:
if the server(s) go down, so does your monitoring system (remember scenario 1
from the HA-lesson?).

Ideally (if the scale of operations make sense, and your tool supports it) you
should cluster your monitoring system to achieve some redundancy.
\subsection*{Reachability}
\label{sec:orgc411140}
Keep in mind that in order to monitor your servers, the monitoring system need
to be able to reach them.

If you for example deploy everything locally and your monitoring on the cloud,
some form of VPN should be in place to allow the monitor to reach the internal
services.
\subsection*{Outsourcing monitoring}
\label{sec:org130d73f}
Some companies buy monitoring as a service. This means their servers and
services are monitored by a third-party supplier.

It's good to know who takes care of monitoring even when not working directly
with it: as an ops person, you need to know who you need to contact in case you
need to take temporarily take down a service, or move it.

It's good practice to let the monitoring people know of upcoming operations that
will trigger alarms, so to spare them the handling of a needless incident.

This may be a matter of courtesy when working with internal monitoring; but a
financial matter when working with an external monitoring solution (usually
monitoring-as-a-service charges extra for each handled alarm).
\section*{How}
\label{sec:org5d97648}
But how does monitoring \emph{actually} work?

There are, roughly speaking, two kinds of monitoring: active and passive.
\subsection*{Passive monitoring}
\label{sec:orga00b139}
Passive monitoring is, as the name implies, based on observations that don't
need the monitored server to actively do anything outside its normal operations.

Example of passive checks:

\begin{itemize}
\item can I ping the host?
\item is the https port reachable and answering?
\item \ldots{}
\end{itemize}
\subsection*{Active monitoring}
\label{sec:orga307866}
Usually active monitoring requires the installation of an \texttt{agent} in the target
server to be monitored. This agent talks back to the monitoring server, giving
information about different parameters (usually configurable).

Examples of data actively collected via an agent:

\begin{itemize}
\item How much memory is the system using?
\item What's the CPU load?
\item How much disk space is there available?
\item When was \texttt{/etc/passwd} last modified?
\item How many docker containers are running in the machine
\item \ldots{}
\end{itemize}
\subsection*{Let's complicate things a bit}
\label{sec:org5680fc5}
To make matter less clear, different systems use the terms "active monitoring"
and "passive monitoring" in other ways.

For example, Zabbix defines active and passive checks as:

In a passive check the agent responds to a data request. Zabbix server (or
proxy) asks for data, for example, CPU load, and Zabbix agent sends back the
result.

Active checks require more complex processing. The agent must first retrieve a
list of items from Zabbix server for independent processing. Then it will
periodically send new values to the server.
\subsection*{Let's complicate things a bit more}
\label{sec:org45fd369}
Lately Prometheus has been gaining popularity in the monitoring game.

It's not based on a server-agent paradigm, but uses instead a time-series
database to collect stats at regular intervals from an HTTP endpoint.
\section*{Alarms}
\label{sec:org3bd7752}
Let's dive deeper in the world of monitoring alarms.

\begin{center}
\includegraphics[width=.9\linewidth]{./noc.jpg}
\end{center}
\subsection*{Generic checks}
\label{sec:org288a2fe}
Most monitoring solutions come pre-loaded with default checks they perform, to
name a few (from Zabbix):

\begin{itemize}
\item hardware: CPU/MEM/disk IO
\item network: throughput, traffic
\item system: uptime, users
\end{itemize}

Those are usually a good start, and cover a lot of scenarios (load too high,
memory too low, disk too low, network bottleneck, system compromise, \ldots{}).
\subsection*{Specific checks}
\label{sec:org126ef19}
On top of generic checks, we can define custom checks, both by loading certain
templates (i.e. Docker template to keep track of containers) and by defining
application checks (i.e. check a certain service on a certain port for
availability, status code, presence of a string or header in the response, \ldots{})

Together with generic checks, specific checks allow us to cover even more
scenarios.
\section*{Tooling}
\label{sec:org63b54ac}
The rest of this lesson will be 2 practical labs we'll use to set up monitoring
for some custom service.
\section*{Labb 1: Zabbix}
\label{sec:org09f0008}
Let's install \href{https://www.zabbix.com}{Zabbix} on a VM.
\subsection*{Zabbix Overview}
\label{sec:org2ef01b8}
\href{https://www.zabbix.com/documentation/current/en/manual/introduction/overview}{See here} for the official documentation.
\subsection*{Install Zabbix}
\label{sec:orge9116f4}
In \href{./zabbix/Vagrantfile}{this Vagrantfile} we can find a 2-VM setup we'll use to install Zabbix.

We'll start with a \texttt{vagrant up}, and later
\href{https://www.zabbix.com/documentation/current/en/manual/installation/install\_from\_packages\#From-distribution-packages}{check
the docs for installation instructions}.
\subsection*{Add a new host}
\label{sec:orgcef21f2}
We'll add a new host by creating a host in the server, and installing and
configuring the agent on the target host.
\subsection*{Trigger a problem}
\label{sec:orge145201}
Next, we'll trigger a problem manually.
\subsection*{Create a web scenario}
\label{sec:org73a3c9e}
We'll create a check for an http service on our target host.

Then we'll start the http service, and check that the web scenario works.

We'll create a trigger for the web scenario, to trigger a problem when the
scenario fails, and try to stop the http service to trigger a problem.
\subsection*{Set up alarms via mail}
\label{sec:orgfe29714}
We'll set up Zabbix to deliver alarms via email.

We'll need to:

\begin{itemize}
\item setup a new media
\item configure the media for our user
\item activate the trigger action under Configuration -> Actions
\item trigger a problem
\end{itemize}
\section*{Labb 2: Prometheus \& Grafana}
\label{sec:org6fbbfe3}
Let's install \href{https://prometheus.io}{Prometheus} on a VM.
\subsection*{Prometheus Overview}
\label{sec:orgffb9a3b}
\href{https://prometheus.io/docs/introduction/overview/}{See here} for the official documentation.
\subsection*{Install Prometheus}
\label{sec:orga6e401e}
See \href{https://prometheus.io/docs/introduction/first\_steps/}{the docs} for installation instructions.

We'll install Prometheus and Grafana.
\subsection*{Add Zabbix as a source to Grafana}
\label{sec:orgb596ca0}
Grafana is a very flexible visualization framework.

We'll install a plugin to allow us to view data from Zabbix in Grafana.
\section*{To be continued}
\label{sec:orgf468da3}
We'll work more with monitoring in the coming lessons.
\end{document}